import { Component, ElementRef, OnInit, ViewChild, AfterViewInit } from "@angular/core";
import { Router } from "@angular/router";
import * as application from "application";
import { ListViewEventData, RadListView } from "nativescript-telerik-ui/listview";
import { BehaviorSubject } from "rxjs/BehaviorSubject";
import { Color } from "color";
import { ObservableArray, ChangedData, ChangeType } from "tns-core-modules/data/observable-array";
import * as Toast from "nativescript-toast";
import { WebView, LoadEventData } from "ui/web-view";


import { AbsoluteLayout } from "ui/layouts/absolute-layout";
import { Label } from "ui/label";
import { Page } from "ui/page";
import { ScrollEventData, ScrollView } from "ui/scroll-view";
import { StackLayout } from "ui/layouts/stack-layout";
import { TextField } from "ui/text-field";
import { View } from "ui/core/view";

import { Item } from "./item";
import { ItemService } from "./item.service";

@Component({
    selector: "ns-items",
    moduleId: module.id,
    templateUrl: "./items.component.html",
    styleUrls: ["./items.component.css"]
})
export class ItemsComponent implements OnInit, AfterViewInit {
    futureItems: BehaviorSubject<Array<Item>>;
    pastItems: BehaviorSubject<Array<Item>>;
    orientationChanged: boolean;
    showHeader: boolean;
    showFooter: boolean;
    footerLocation: number;
    headerLocation: number;
    tappedItem: number;
    absoluteLayoutObj: AbsoluteLayout
    radList1Obj: RadListView;
    radList2Obj: RadListView;
    scrollViewObj: ScrollView;
    stackLayout1Obj: StackLayout;
    stackLayout2Obj: StackLayout;

    public webViewSrc: string = "https://www.nativescript.org/";

    @ViewChild("myWebView") webViewRef: ElementRef;
    @ViewChild("urlField") urlFieldRef: ElementRef;
    @ViewChild("labelResult") labelResultRef: ElementRef;

    @ViewChild("absoluteLayout") absoluteLayout: ElementRef;
    @ViewChild("radList1") radlist1: ElementRef;
    @ViewChild("radList2") radlist2: ElementRef;
    @ViewChild("scrollView") scrollView: ElementRef;
    @ViewChild("stackLayout1") stackLayout1: ElementRef;
    @ViewChild("stackLayout2") stackLayout2: ElementRef;

    constructor(
        private itemService: ItemService,
        private page: Page,
        private router: Router
        ) { }

    ngOnInit(): void {
        //Initialising properties
        this.showFooter = false;
        this.showHeader = false;
        this.footerLocation = 0;
        this.headerLocation = 0;
        this.tappedItem = -1;
        this.page.actionBarHidden = true;
        //Fetching the data items
        this.itemService.getItems()
        .then((items) => {
            this.futureItems = new BehaviorSubject(items.filter(item => item.type === "future"));
        });
        this.itemService.getItems()
        .then((items) => {
            this.pastItems = new BehaviorSubject(items.filter(item => item.type === "past"));
        });
    }

    ngAfterViewInit() {
        this.absoluteLayoutObj = <AbsoluteLayout> this.absoluteLayout.nativeElement;
        this.radList1Obj = <RadListView> this.radlist1.nativeElement;
        this.radList2Obj = <RadListView> this.radlist2.nativeElement;
        this.scrollViewObj = <ScrollView> this.scrollView.nativeElement;
        this.stackLayout1Obj = <StackLayout> this.stackLayout1.nativeElement;
        this.stackLayout2Obj = <StackLayout> this.stackLayout2.nativeElement;

        this.page.on(Page.navigatedFromEvent, function() {
            this.removeEventListeners();
            console.log("Navigated from items-component...");
        }, this);

        this.page.on(Page.navigatedToEvent, function() {
            this.setEventListeners();
            console.log("Navigated to items-component...");
        }, this);

        let webview: WebView = this.webViewRef.nativeElement;
        let label: Label = this.labelResultRef.nativeElement;
        label.text = "WebView is still loading...";

        webview.on(WebView.loadFinishedEvent, function (args: LoadEventData) {
            let message;
            if (!args.error) {
                message = "WebView finished loading of " + args.url;
            } else {
                message = "Error loading " + args.url + ": " + args.error;
            }

            label.text = message;
            console.log("WebView message - " + message);
        });
    }

    public onScroll(args: ScrollEventData) {
        //Logic for displaying header and footer
        this.setHeaderAndFooterLocation();

        //To handle orientation changes
        if (this.orientationChanged) {
            //Location changes in order to hide header and footer
            let that = this;
            setTimeout(function() {
                that.headerLocation = -100;
                that.footerLocation = -100;
                that.setHeaderAndFooterLocation();
            }, 100); //Called with a delay to prevent re-location problems
            this.orientationChanged = false;
            console.log("Orientation changes set...");
        }
        console.log("scrollY: " + args.scrollY);
    }

    setHeaderAndFooterLocation() {
        //Header location logic
        if (this.stackLayout2Obj.getLocationRelativeTo(this.scrollViewObj).y < this.scrollViewObj.getLocationRelativeTo(this.absoluteLayoutObj).y) {
            this.showHeader = true;
            this.headerLocation = this.scrollViewObj.getLocationRelativeTo(this.absoluteLayoutObj).y;
        } else {
            this.showHeader = false;
        }

        //Footer location Logic
        if (this.stackLayout1Obj.getLocationRelativeTo(this.scrollViewObj).y > (this.scrollViewObj.getActualSize().height - this.stackLayout1Obj.getActualSize().height)) {
            this.showFooter = true;
            this.footerLocation = this.scrollViewObj.getActualSize().height - this.stackLayout1Obj.getActualSize().height;
        } else {
            this.showFooter = false;
        }
    }

    onItemTapFuture(args: ListViewEventData) {
        //To handle futureItems tap events
        let items = this.futureItems.getValue();
        this.tappedItem = items[args.itemIndex].id;
        let that = this;
        setTimeout(function() {
            that.tappedItem = -1;
            that.router.navigate(["/item", items[args.itemIndex].id]);
        }, 10);
        console.log("Future item tapped");
    }

    onItemTapPast(args: ListViewEventData) {
        //To handle pastItems tap events
        let items = this.pastItems.getValue();
        this.tappedItem = items[args.itemIndex].id;
        let that = this;
        setTimeout(function() {
            that.tappedItem = -1;
            that.router.navigate(["/item", items[args.itemIndex].id]);
        }, 10);
        console.log("Past item tapped");
    }

    showLog(message: string) {
        //To handle header and footer tap events
        console.log(message);
    }

    setEventListeners() {
        //Rebinding the list items
        application.on(application.resumeEvent, function() {
            //This is necessary so as to restore the tap event bindings
            application.on(application.orientationChangedEvent, function() {
                /*
                    This is necessary as when the orientation changes, 
                    the footer/header might not get re-located properly.
                */
                let scroll1 = this.radList1Obj.getLocationRelativeTo(this.scrollViewObj).y;
                if (scroll1 < 0) {
                    this.scrollViewObj.scrollToVerticalOffset(Math.abs(scroll1 + 1));
                } else {
                    this.scrollViewObj.scrollToVerticalOffset(scroll1+1);
                }
                this.orientationChanged = true;
                console.log("Orientation changed...");
            }, this);
            this.radList1Obj.refresh();
            this.radList2Obj.refresh();
            console.log("Application resumed...");
        }, this);

        application.on(application.suspendEvent, function() {
            application.off(application.orientationChangedEvent);
            console.log("Application suspended...");
        }, this);

        //Handling orientation changes
        application.on(application.orientationChangedEvent, function() {
            /*
                This is necessary as when the orientation changes, 
                the footer/header might not get re-located properly.
            */
            let scroll1 = this.radList1Obj.getLocationRelativeTo(this.scrollViewObj).y;
            if (scroll1 < 0) {
                this.scrollViewObj.scrollToVerticalOffset(Math.abs(scroll1 + 1));
            } else {
                this.scrollViewObj.scrollToVerticalOffset(scroll1+1);
            }
            this.orientationChanged = true;
            console.log("Orientation changed...");
        }, this);
    }

    removeEventListeners() {
        application.off(application.orientationChangedEvent);
        application.off(application.resumeEvent);
        application.off(application.suspendEvent);
    }


    goBack() {
        let webview: WebView = this.webViewRef.nativeElement;
        if (webview.canGoBack) {
            webview.goBack();
        }
    }

    submit(args: string) {
        let textField: TextField = this.urlFieldRef.nativeElement;

        if (args.substring(0, 4) === "http") {
            this.webViewSrc = args;
            textField.dismissSoftInput();
        } else {
            alert("Please, add `http://` or `https://` in front of the URL string");
        }
    }
}
