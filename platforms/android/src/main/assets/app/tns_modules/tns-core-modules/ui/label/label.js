function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
var text_base_1 = require("../text-base");
__export(require("../text-base"));
var Label = (function (_super) {
    __extends(Label, _super);
    function Label() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Object.defineProperty(Label.prototype, "textWrap", {
        get: function () {
            return this.style.whiteSpace === text_base_1.WhiteSpace.NORMAL;
        },
        set: function (value) {
            this.style.whiteSpace = value ? text_base_1.WhiteSpace.NORMAL : text_base_1.WhiteSpace.NO_WRAP;
        },
        enumerable: true,
        configurable: true
    });
    Label.prototype.createNativeView = function () {
        var textView = new android.widget.TextView(this._context);
        textView.setSingleLine(true);
        textView.setEllipsize(android.text.TextUtils.TruncateAt.END);
        return textView;
    };
    Label.prototype.initNativeView = function () {
        _super.prototype.initNativeView.call(this);
        var textView = this.nativeView;
        textView.setSingleLine(true);
        textView.setEllipsize(android.text.TextUtils.TruncateAt.END);
    };
    return Label;
}(text_base_1.TextBase));
exports.Label = Label;
//# sourceMappingURL=label.js.map