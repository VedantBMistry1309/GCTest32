"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var application = require("application");
var BehaviorSubject_1 = require("rxjs/BehaviorSubject");
var web_view_1 = require("ui/web-view");
var page_1 = require("ui/page");
var item_service_1 = require("./item.service");
var ItemsComponent = (function () {
    function ItemsComponent(itemService, page, router) {
        this.itemService = itemService;
        this.page = page;
        this.router = router;
        this.webViewSrc = "https://www.nativescript.org/";
    }
    ItemsComponent.prototype.ngOnInit = function () {
        var _this = this;
        //Initialising properties
        this.showFooter = false;
        this.showHeader = false;
        this.footerLocation = 0;
        this.headerLocation = 0;
        this.tappedItem = -1;
        this.page.actionBarHidden = true;
        //Fetching the data items
        this.itemService.getItems()
            .then(function (items) {
            _this.futureItems = new BehaviorSubject_1.BehaviorSubject(items.filter(function (item) { return item.type === "future"; }));
        });
        this.itemService.getItems()
            .then(function (items) {
            _this.pastItems = new BehaviorSubject_1.BehaviorSubject(items.filter(function (item) { return item.type === "past"; }));
        });
    };
    ItemsComponent.prototype.ngAfterViewInit = function () {
        this.absoluteLayoutObj = this.absoluteLayout.nativeElement;
        this.radList1Obj = this.radlist1.nativeElement;
        this.radList2Obj = this.radlist2.nativeElement;
        this.scrollViewObj = this.scrollView.nativeElement;
        this.stackLayout1Obj = this.stackLayout1.nativeElement;
        this.stackLayout2Obj = this.stackLayout2.nativeElement;
        this.page.on(page_1.Page.navigatedFromEvent, function () {
            this.removeEventListeners();
            console.log("Navigated from items-component...");
        }, this);
        this.page.on(page_1.Page.navigatedToEvent, function () {
            this.setEventListeners();
            console.log("Navigated to items-component...");
        }, this);
        var webview = this.webViewRef.nativeElement;
        var label = this.labelResultRef.nativeElement;
        label.text = "WebView is still loading...";
        webview.on(web_view_1.WebView.loadFinishedEvent, function (args) {
            var message;
            if (!args.error) {
                message = "WebView finished loading of " + args.url;
            }
            else {
                message = "Error loading " + args.url + ": " + args.error;
            }
            label.text = message;
            console.log("WebView message - " + message);
        });
    };
    ItemsComponent.prototype.onScroll = function (args) {
        //Logic for displaying header and footer
        this.setHeaderAndFooterLocation();
        //To handle orientation changes
        if (this.orientationChanged) {
            //Location changes in order to hide header and footer
            var that_1 = this;
            setTimeout(function () {
                that_1.headerLocation = -100;
                that_1.footerLocation = -100;
                that_1.setHeaderAndFooterLocation();
            }, 100); //Called with a delay to prevent re-location problems
            this.orientationChanged = false;
            console.log("Orientation changes set...");
        }
        console.log("scrollY: " + args.scrollY);
    };
    ItemsComponent.prototype.setHeaderAndFooterLocation = function () {
        //Header location logic
        if (this.stackLayout2Obj.getLocationRelativeTo(this.scrollViewObj).y < this.scrollViewObj.getLocationRelativeTo(this.absoluteLayoutObj).y) {
            this.showHeader = true;
            this.headerLocation = this.scrollViewObj.getLocationRelativeTo(this.absoluteLayoutObj).y;
        }
        else {
            this.showHeader = false;
        }
        //Footer location Logic
        if (this.stackLayout1Obj.getLocationRelativeTo(this.scrollViewObj).y > (this.scrollViewObj.getActualSize().height - this.stackLayout1Obj.getActualSize().height)) {
            this.showFooter = true;
            this.footerLocation = this.scrollViewObj.getActualSize().height - this.stackLayout1Obj.getActualSize().height;
        }
        else {
            this.showFooter = false;
        }
    };
    ItemsComponent.prototype.onItemTapFuture = function (args) {
        //To handle futureItems tap events
        var items = this.futureItems.getValue();
        this.tappedItem = items[args.itemIndex].id;
        var that = this;
        setTimeout(function () {
            that.tappedItem = -1;
            that.router.navigate(["/item", items[args.itemIndex].id]);
        }, 10);
        console.log("Future item tapped");
    };
    ItemsComponent.prototype.onItemTapPast = function (args) {
        //To handle pastItems tap events
        var items = this.pastItems.getValue();
        this.tappedItem = items[args.itemIndex].id;
        var that = this;
        setTimeout(function () {
            that.tappedItem = -1;
            that.router.navigate(["/item", items[args.itemIndex].id]);
        }, 10);
        console.log("Past item tapped");
    };
    ItemsComponent.prototype.showLog = function (message) {
        //To handle header and footer tap events
        console.log(message);
    };
    ItemsComponent.prototype.setEventListeners = function () {
        //Rebinding the list items
        application.on(application.resumeEvent, function () {
            //This is necessary so as to restore the tap event bindings
            application.on(application.orientationChangedEvent, function () {
                /*
                    This is necessary as when the orientation changes,
                    the footer/header might not get re-located properly.
                */
                var scroll1 = this.radList1Obj.getLocationRelativeTo(this.scrollViewObj).y;
                if (scroll1 < 0) {
                    this.scrollViewObj.scrollToVerticalOffset(Math.abs(scroll1 + 1));
                }
                else {
                    this.scrollViewObj.scrollToVerticalOffset(scroll1 + 1);
                }
                this.orientationChanged = true;
                console.log("Orientation changed...");
            }, this);
            this.radList1Obj.refresh();
            this.radList2Obj.refresh();
            console.log("Application resumed...");
        }, this);
        application.on(application.suspendEvent, function () {
            application.off(application.orientationChangedEvent);
            console.log("Application suspended...");
        }, this);
        //Handling orientation changes
        application.on(application.orientationChangedEvent, function () {
            /*
                This is necessary as when the orientation changes,
                the footer/header might not get re-located properly.
            */
            var scroll1 = this.radList1Obj.getLocationRelativeTo(this.scrollViewObj).y;
            if (scroll1 < 0) {
                this.scrollViewObj.scrollToVerticalOffset(Math.abs(scroll1 + 1));
            }
            else {
                this.scrollViewObj.scrollToVerticalOffset(scroll1 + 1);
            }
            this.orientationChanged = true;
            console.log("Orientation changed...");
        }, this);
    };
    ItemsComponent.prototype.removeEventListeners = function () {
        application.off(application.orientationChangedEvent);
        application.off(application.resumeEvent);
        application.off(application.suspendEvent);
    };
    ItemsComponent.prototype.goBack = function () {
        var webview = this.webViewRef.nativeElement;
        if (webview.canGoBack) {
            webview.goBack();
        }
    };
    ItemsComponent.prototype.submit = function (args) {
        var textField = this.urlFieldRef.nativeElement;
        if (args.substring(0, 4) === "http") {
            this.webViewSrc = args;
            textField.dismissSoftInput();
        }
        else {
            alert("Please, add `http://` or `https://` in front of the URL string");
        }
    };
    return ItemsComponent;
}());
__decorate([
    core_1.ViewChild("myWebView"),
    __metadata("design:type", core_1.ElementRef)
], ItemsComponent.prototype, "webViewRef", void 0);
__decorate([
    core_1.ViewChild("urlField"),
    __metadata("design:type", core_1.ElementRef)
], ItemsComponent.prototype, "urlFieldRef", void 0);
__decorate([
    core_1.ViewChild("labelResult"),
    __metadata("design:type", core_1.ElementRef)
], ItemsComponent.prototype, "labelResultRef", void 0);
__decorate([
    core_1.ViewChild("absoluteLayout"),
    __metadata("design:type", core_1.ElementRef)
], ItemsComponent.prototype, "absoluteLayout", void 0);
__decorate([
    core_1.ViewChild("radList1"),
    __metadata("design:type", core_1.ElementRef)
], ItemsComponent.prototype, "radlist1", void 0);
__decorate([
    core_1.ViewChild("radList2"),
    __metadata("design:type", core_1.ElementRef)
], ItemsComponent.prototype, "radlist2", void 0);
__decorate([
    core_1.ViewChild("scrollView"),
    __metadata("design:type", core_1.ElementRef)
], ItemsComponent.prototype, "scrollView", void 0);
__decorate([
    core_1.ViewChild("stackLayout1"),
    __metadata("design:type", core_1.ElementRef)
], ItemsComponent.prototype, "stackLayout1", void 0);
__decorate([
    core_1.ViewChild("stackLayout2"),
    __metadata("design:type", core_1.ElementRef)
], ItemsComponent.prototype, "stackLayout2", void 0);
ItemsComponent = __decorate([
    core_1.Component({
        selector: "ns-items",
        moduleId: module.id,
        templateUrl: "./items.component.html",
        styleUrls: ["./items.component.css"]
    }),
    __metadata("design:paramtypes", [item_service_1.ItemService,
        page_1.Page,
        router_1.Router])
], ItemsComponent);
exports.ItemsComponent = ItemsComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaXRlbXMuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiaXRlbXMuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsc0NBQXdGO0FBQ3hGLDBDQUF5QztBQUN6Qyx5Q0FBMkM7QUFFM0Msd0RBQXVEO0FBSXZELHdDQUFxRDtBQUtyRCxnQ0FBK0I7QUFPL0IsK0NBQTZDO0FBUTdDLElBQWEsY0FBYztJQTZCdkIsd0JBQ1ksV0FBd0IsRUFDeEIsSUFBVSxFQUNWLE1BQWM7UUFGZCxnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUN4QixTQUFJLEdBQUosSUFBSSxDQUFNO1FBQ1YsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQWhCbkIsZUFBVSxHQUFXLCtCQUErQixDQUFDO0lBaUJwRCxDQUFDO0lBRVQsaUNBQVEsR0FBUjtRQUFBLGlCQWlCQztRQWhCRyx5QkFBeUI7UUFDekIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7UUFDeEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7UUFDeEIsSUFBSSxDQUFDLGNBQWMsR0FBRyxDQUFDLENBQUM7UUFDeEIsSUFBSSxDQUFDLGNBQWMsR0FBRyxDQUFDLENBQUM7UUFDeEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxDQUFDLENBQUMsQ0FBQztRQUNyQixJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUM7UUFDakMseUJBQXlCO1FBQ3pCLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxFQUFFO2FBQzFCLElBQUksQ0FBQyxVQUFDLEtBQUs7WUFDUixLQUFJLENBQUMsV0FBVyxHQUFHLElBQUksaUNBQWUsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLFVBQUEsSUFBSSxJQUFJLE9BQUEsSUFBSSxDQUFDLElBQUksS0FBSyxRQUFRLEVBQXRCLENBQXNCLENBQUMsQ0FBQyxDQUFDO1FBQ3pGLENBQUMsQ0FBQyxDQUFDO1FBQ0gsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLEVBQUU7YUFDMUIsSUFBSSxDQUFDLFVBQUMsS0FBSztZQUNSLEtBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxpQ0FBZSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsVUFBQSxJQUFJLElBQUksT0FBQSxJQUFJLENBQUMsSUFBSSxLQUFLLE1BQU0sRUFBcEIsQ0FBb0IsQ0FBQyxDQUFDLENBQUM7UUFDckYsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQsd0NBQWUsR0FBZjtRQUNJLElBQUksQ0FBQyxpQkFBaUIsR0FBb0IsSUFBSSxDQUFDLGNBQWMsQ0FBQyxhQUFhLENBQUM7UUFDNUUsSUFBSSxDQUFDLFdBQVcsR0FBaUIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUM7UUFDN0QsSUFBSSxDQUFDLFdBQVcsR0FBaUIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUM7UUFDN0QsSUFBSSxDQUFDLGFBQWEsR0FBZ0IsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUM7UUFDaEUsSUFBSSxDQUFDLGVBQWUsR0FBaUIsSUFBSSxDQUFDLFlBQVksQ0FBQyxhQUFhLENBQUM7UUFDckUsSUFBSSxDQUFDLGVBQWUsR0FBaUIsSUFBSSxDQUFDLFlBQVksQ0FBQyxhQUFhLENBQUM7UUFFckUsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsV0FBSSxDQUFDLGtCQUFrQixFQUFFO1lBQ2xDLElBQUksQ0FBQyxvQkFBb0IsRUFBRSxDQUFDO1lBQzVCLE9BQU8sQ0FBQyxHQUFHLENBQUMsbUNBQW1DLENBQUMsQ0FBQztRQUNyRCxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFFVCxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxXQUFJLENBQUMsZ0JBQWdCLEVBQUU7WUFDaEMsSUFBSSxDQUFDLGlCQUFpQixFQUFFLENBQUM7WUFDekIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxpQ0FBaUMsQ0FBQyxDQUFDO1FBQ25ELENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUVULElBQUksT0FBTyxHQUFZLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDO1FBQ3JELElBQUksS0FBSyxHQUFVLElBQUksQ0FBQyxjQUFjLENBQUMsYUFBYSxDQUFDO1FBQ3JELEtBQUssQ0FBQyxJQUFJLEdBQUcsNkJBQTZCLENBQUM7UUFFM0MsT0FBTyxDQUFDLEVBQUUsQ0FBQyxrQkFBTyxDQUFDLGlCQUFpQixFQUFFLFVBQVUsSUFBbUI7WUFDL0QsSUFBSSxPQUFPLENBQUM7WUFDWixFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO2dCQUNkLE9BQU8sR0FBRyw4QkFBOEIsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDO1lBQ3hELENBQUM7WUFBQyxJQUFJLENBQUMsQ0FBQztnQkFDSixPQUFPLEdBQUcsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLEdBQUcsR0FBRyxJQUFJLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQztZQUM5RCxDQUFDO1lBRUQsS0FBSyxDQUFDLElBQUksR0FBRyxPQUFPLENBQUM7WUFDckIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxvQkFBb0IsR0FBRyxPQUFPLENBQUMsQ0FBQztRQUNoRCxDQUFDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFTSxpQ0FBUSxHQUFmLFVBQWdCLElBQXFCO1FBQ2pDLHdDQUF3QztRQUN4QyxJQUFJLENBQUMsMEJBQTBCLEVBQUUsQ0FBQztRQUVsQywrQkFBK0I7UUFDL0IsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLENBQUMsQ0FBQztZQUMxQixxREFBcUQ7WUFDckQsSUFBSSxNQUFJLEdBQUcsSUFBSSxDQUFDO1lBQ2hCLFVBQVUsQ0FBQztnQkFDUCxNQUFJLENBQUMsY0FBYyxHQUFHLENBQUMsR0FBRyxDQUFDO2dCQUMzQixNQUFJLENBQUMsY0FBYyxHQUFHLENBQUMsR0FBRyxDQUFDO2dCQUMzQixNQUFJLENBQUMsMEJBQTBCLEVBQUUsQ0FBQztZQUN0QyxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUMsQ0FBQyxxREFBcUQ7WUFDOUQsSUFBSSxDQUFDLGtCQUFrQixHQUFHLEtBQUssQ0FBQztZQUNoQyxPQUFPLENBQUMsR0FBRyxDQUFDLDRCQUE0QixDQUFDLENBQUM7UUFDOUMsQ0FBQztRQUNELE9BQU8sQ0FBQyxHQUFHLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUM1QyxDQUFDO0lBRUQsbURBQTBCLEdBQTFCO1FBQ0ksdUJBQXVCO1FBQ3ZCLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMscUJBQXFCLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDeEksSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7WUFDdkIsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUM3RixDQUFDO1FBQUMsSUFBSSxDQUFDLENBQUM7WUFDSixJQUFJLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQztRQUM1QixDQUFDO1FBRUQsdUJBQXVCO1FBQ3ZCLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMscUJBQXFCLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsYUFBYSxFQUFFLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsYUFBYSxFQUFFLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQy9KLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO1lBQ3ZCLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxhQUFhLEVBQUUsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxhQUFhLEVBQUUsQ0FBQyxNQUFNLENBQUM7UUFDbEgsQ0FBQztRQUFDLElBQUksQ0FBQyxDQUFDO1lBQ0osSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7UUFDNUIsQ0FBQztJQUNMLENBQUM7SUFFRCx3Q0FBZSxHQUFmLFVBQWdCLElBQXVCO1FBQ25DLGtDQUFrQztRQUNsQyxJQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsRUFBRSxDQUFDO1FBQ3hDLElBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDM0MsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBQ2hCLFVBQVUsQ0FBQztZQUNQLElBQUksQ0FBQyxVQUFVLEdBQUcsQ0FBQyxDQUFDLENBQUM7WUFDckIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxPQUFPLEVBQUUsS0FBSyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQzlELENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQztRQUNQLE9BQU8sQ0FBQyxHQUFHLENBQUMsb0JBQW9CLENBQUMsQ0FBQztJQUN0QyxDQUFDO0lBRUQsc0NBQWEsR0FBYixVQUFjLElBQXVCO1FBQ2pDLGdDQUFnQztRQUNoQyxJQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsRUFBRSxDQUFDO1FBQ3RDLElBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDM0MsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBQ2hCLFVBQVUsQ0FBQztZQUNQLElBQUksQ0FBQyxVQUFVLEdBQUcsQ0FBQyxDQUFDLENBQUM7WUFDckIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxPQUFPLEVBQUUsS0FBSyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQzlELENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQztRQUNQLE9BQU8sQ0FBQyxHQUFHLENBQUMsa0JBQWtCLENBQUMsQ0FBQztJQUNwQyxDQUFDO0lBRUQsZ0NBQU8sR0FBUCxVQUFRLE9BQWU7UUFDbkIsd0NBQXdDO1FBQ3hDLE9BQU8sQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDekIsQ0FBQztJQUVELDBDQUFpQixHQUFqQjtRQUNJLDBCQUEwQjtRQUMxQixXQUFXLENBQUMsRUFBRSxDQUFDLFdBQVcsQ0FBQyxXQUFXLEVBQUU7WUFDcEMsMkRBQTJEO1lBQzNELFdBQVcsQ0FBQyxFQUFFLENBQUMsV0FBVyxDQUFDLHVCQUF1QixFQUFFO2dCQUNoRDs7O2tCQUdFO2dCQUNGLElBQUksT0FBTyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMscUJBQXFCLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDM0UsRUFBRSxDQUFDLENBQUMsT0FBTyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQ2QsSUFBSSxDQUFDLGFBQWEsQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLE9BQU8sR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNyRSxDQUFDO2dCQUFDLElBQUksQ0FBQyxDQUFDO29CQUNKLElBQUksQ0FBQyxhQUFhLENBQUMsc0JBQXNCLENBQUMsT0FBTyxHQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUN6RCxDQUFDO2dCQUNELElBQUksQ0FBQyxrQkFBa0IsR0FBRyxJQUFJLENBQUM7Z0JBQy9CLE9BQU8sQ0FBQyxHQUFHLENBQUMsd0JBQXdCLENBQUMsQ0FBQztZQUMxQyxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7WUFDVCxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sRUFBRSxDQUFDO1lBQzNCLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxFQUFFLENBQUM7WUFDM0IsT0FBTyxDQUFDLEdBQUcsQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDO1FBQzFDLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUVULFdBQVcsQ0FBQyxFQUFFLENBQUMsV0FBVyxDQUFDLFlBQVksRUFBRTtZQUNyQyxXQUFXLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDO1lBQ3JELE9BQU8sQ0FBQyxHQUFHLENBQUMsMEJBQTBCLENBQUMsQ0FBQztRQUM1QyxDQUFDLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFFVCw4QkFBOEI7UUFDOUIsV0FBVyxDQUFDLEVBQUUsQ0FBQyxXQUFXLENBQUMsdUJBQXVCLEVBQUU7WUFDaEQ7OztjQUdFO1lBQ0YsSUFBSSxPQUFPLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzNFLEVBQUUsQ0FBQyxDQUFDLE9BQU8sR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUNkLElBQUksQ0FBQyxhQUFhLENBQUMsc0JBQXNCLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxPQUFPLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNyRSxDQUFDO1lBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ0osSUFBSSxDQUFDLGFBQWEsQ0FBQyxzQkFBc0IsQ0FBQyxPQUFPLEdBQUMsQ0FBQyxDQUFDLENBQUM7WUFDekQsQ0FBQztZQUNELElBQUksQ0FBQyxrQkFBa0IsR0FBRyxJQUFJLENBQUM7WUFDL0IsT0FBTyxDQUFDLEdBQUcsQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDO1FBQzFDLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztJQUNiLENBQUM7SUFFRCw2Q0FBb0IsR0FBcEI7UUFDSSxXQUFXLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyx1QkFBdUIsQ0FBQyxDQUFDO1FBQ3JELFdBQVcsQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQ3pDLFdBQVcsQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyxDQUFDO0lBQzlDLENBQUM7SUFHRCwrQkFBTSxHQUFOO1FBQ0ksSUFBSSxPQUFPLEdBQVksSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUM7UUFDckQsRUFBRSxDQUFDLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7WUFDcEIsT0FBTyxDQUFDLE1BQU0sRUFBRSxDQUFDO1FBQ3JCLENBQUM7SUFDTCxDQUFDO0lBRUQsK0JBQU0sR0FBTixVQUFPLElBQVk7UUFDZixJQUFJLFNBQVMsR0FBYyxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsQ0FBQztRQUUxRCxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsS0FBSyxNQUFNLENBQUMsQ0FBQyxDQUFDO1lBQ2xDLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO1lBQ3ZCLFNBQVMsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO1FBQ2pDLENBQUM7UUFBQyxJQUFJLENBQUMsQ0FBQztZQUNKLEtBQUssQ0FBQyxnRUFBZ0UsQ0FBQyxDQUFDO1FBQzVFLENBQUM7SUFDTCxDQUFDO0lBQ0wscUJBQUM7QUFBRCxDQUFDLEFBaE9ELElBZ09DO0FBOU0yQjtJQUF2QixnQkFBUyxDQUFDLFdBQVcsQ0FBQzs4QkFBYSxpQkFBVTtrREFBQztBQUN4QjtJQUF0QixnQkFBUyxDQUFDLFVBQVUsQ0FBQzs4QkFBYyxpQkFBVTttREFBQztBQUNyQjtJQUF6QixnQkFBUyxDQUFDLGFBQWEsQ0FBQzs4QkFBaUIsaUJBQVU7c0RBQUM7QUFFeEI7SUFBNUIsZ0JBQVMsQ0FBQyxnQkFBZ0IsQ0FBQzs4QkFBaUIsaUJBQVU7c0RBQUM7QUFDakM7SUFBdEIsZ0JBQVMsQ0FBQyxVQUFVLENBQUM7OEJBQVcsaUJBQVU7Z0RBQUM7QUFDckI7SUFBdEIsZ0JBQVMsQ0FBQyxVQUFVLENBQUM7OEJBQVcsaUJBQVU7Z0RBQUM7QUFDbkI7SUFBeEIsZ0JBQVMsQ0FBQyxZQUFZLENBQUM7OEJBQWEsaUJBQVU7a0RBQUM7QUFDckI7SUFBMUIsZ0JBQVMsQ0FBQyxjQUFjLENBQUM7OEJBQWUsaUJBQVU7b0RBQUM7QUFDekI7SUFBMUIsZ0JBQVMsQ0FBQyxjQUFjLENBQUM7OEJBQWUsaUJBQVU7b0RBQUM7QUEzQjNDLGNBQWM7SUFOMUIsZ0JBQVMsQ0FBQztRQUNQLFFBQVEsRUFBRSxVQUFVO1FBQ3BCLFFBQVEsRUFBRSxNQUFNLENBQUMsRUFBRTtRQUNuQixXQUFXLEVBQUUsd0JBQXdCO1FBQ3JDLFNBQVMsRUFBRSxDQUFDLHVCQUF1QixDQUFDO0tBQ3ZDLENBQUM7cUNBK0IyQiwwQkFBVztRQUNsQixXQUFJO1FBQ0YsZUFBTTtHQWhDakIsY0FBYyxDQWdPMUI7QUFoT1ksd0NBQWMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIEVsZW1lbnRSZWYsIE9uSW5pdCwgVmlld0NoaWxkLCBBZnRlclZpZXdJbml0IH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcbmltcG9ydCB7IFJvdXRlciB9IGZyb20gXCJAYW5ndWxhci9yb3V0ZXJcIjtcbmltcG9ydCAqIGFzIGFwcGxpY2F0aW9uIGZyb20gXCJhcHBsaWNhdGlvblwiO1xuaW1wb3J0IHsgTGlzdFZpZXdFdmVudERhdGEsIFJhZExpc3RWaWV3IH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC10ZWxlcmlrLXVpL2xpc3R2aWV3XCI7XG5pbXBvcnQgeyBCZWhhdmlvclN1YmplY3QgfSBmcm9tIFwicnhqcy9CZWhhdmlvclN1YmplY3RcIjtcbmltcG9ydCB7IENvbG9yIH0gZnJvbSBcImNvbG9yXCI7XG5pbXBvcnQgeyBPYnNlcnZhYmxlQXJyYXksIENoYW5nZWREYXRhLCBDaGFuZ2VUeXBlIH0gZnJvbSBcInRucy1jb3JlLW1vZHVsZXMvZGF0YS9vYnNlcnZhYmxlLWFycmF5XCI7XG5pbXBvcnQgKiBhcyBUb2FzdCBmcm9tIFwibmF0aXZlc2NyaXB0LXRvYXN0XCI7XG5pbXBvcnQgeyBXZWJWaWV3LCBMb2FkRXZlbnREYXRhIH0gZnJvbSBcInVpL3dlYi12aWV3XCI7XG5cblxuaW1wb3J0IHsgQWJzb2x1dGVMYXlvdXQgfSBmcm9tIFwidWkvbGF5b3V0cy9hYnNvbHV0ZS1sYXlvdXRcIjtcbmltcG9ydCB7IExhYmVsIH0gZnJvbSBcInVpL2xhYmVsXCI7XG5pbXBvcnQgeyBQYWdlIH0gZnJvbSBcInVpL3BhZ2VcIjtcbmltcG9ydCB7IFNjcm9sbEV2ZW50RGF0YSwgU2Nyb2xsVmlldyB9IGZyb20gXCJ1aS9zY3JvbGwtdmlld1wiO1xuaW1wb3J0IHsgU3RhY2tMYXlvdXQgfSBmcm9tIFwidWkvbGF5b3V0cy9zdGFjay1sYXlvdXRcIjtcbmltcG9ydCB7IFRleHRGaWVsZCB9IGZyb20gXCJ1aS90ZXh0LWZpZWxkXCI7XG5pbXBvcnQgeyBWaWV3IH0gZnJvbSBcInVpL2NvcmUvdmlld1wiO1xuXG5pbXBvcnQgeyBJdGVtIH0gZnJvbSBcIi4vaXRlbVwiO1xuaW1wb3J0IHsgSXRlbVNlcnZpY2UgfSBmcm9tIFwiLi9pdGVtLnNlcnZpY2VcIjtcblxuQENvbXBvbmVudCh7XG4gICAgc2VsZWN0b3I6IFwibnMtaXRlbXNcIixcbiAgICBtb2R1bGVJZDogbW9kdWxlLmlkLFxuICAgIHRlbXBsYXRlVXJsOiBcIi4vaXRlbXMuY29tcG9uZW50Lmh0bWxcIixcbiAgICBzdHlsZVVybHM6IFtcIi4vaXRlbXMuY29tcG9uZW50LmNzc1wiXVxufSlcbmV4cG9ydCBjbGFzcyBJdGVtc0NvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgQWZ0ZXJWaWV3SW5pdCB7XG4gICAgZnV0dXJlSXRlbXM6IEJlaGF2aW9yU3ViamVjdDxBcnJheTxJdGVtPj47XG4gICAgcGFzdEl0ZW1zOiBCZWhhdmlvclN1YmplY3Q8QXJyYXk8SXRlbT4+O1xuICAgIG9yaWVudGF0aW9uQ2hhbmdlZDogYm9vbGVhbjtcbiAgICBzaG93SGVhZGVyOiBib29sZWFuO1xuICAgIHNob3dGb290ZXI6IGJvb2xlYW47XG4gICAgZm9vdGVyTG9jYXRpb246IG51bWJlcjtcbiAgICBoZWFkZXJMb2NhdGlvbjogbnVtYmVyO1xuICAgIHRhcHBlZEl0ZW06IG51bWJlcjtcbiAgICBhYnNvbHV0ZUxheW91dE9iajogQWJzb2x1dGVMYXlvdXRcbiAgICByYWRMaXN0MU9iajogUmFkTGlzdFZpZXc7XG4gICAgcmFkTGlzdDJPYmo6IFJhZExpc3RWaWV3O1xuICAgIHNjcm9sbFZpZXdPYmo6IFNjcm9sbFZpZXc7XG4gICAgc3RhY2tMYXlvdXQxT2JqOiBTdGFja0xheW91dDtcbiAgICBzdGFja0xheW91dDJPYmo6IFN0YWNrTGF5b3V0O1xuXG4gICAgcHVibGljIHdlYlZpZXdTcmM6IHN0cmluZyA9IFwiaHR0cHM6Ly93d3cubmF0aXZlc2NyaXB0Lm9yZy9cIjtcblxuICAgIEBWaWV3Q2hpbGQoXCJteVdlYlZpZXdcIikgd2ViVmlld1JlZjogRWxlbWVudFJlZjtcbiAgICBAVmlld0NoaWxkKFwidXJsRmllbGRcIikgdXJsRmllbGRSZWY6IEVsZW1lbnRSZWY7XG4gICAgQFZpZXdDaGlsZChcImxhYmVsUmVzdWx0XCIpIGxhYmVsUmVzdWx0UmVmOiBFbGVtZW50UmVmO1xuXG4gICAgQFZpZXdDaGlsZChcImFic29sdXRlTGF5b3V0XCIpIGFic29sdXRlTGF5b3V0OiBFbGVtZW50UmVmO1xuICAgIEBWaWV3Q2hpbGQoXCJyYWRMaXN0MVwiKSByYWRsaXN0MTogRWxlbWVudFJlZjtcbiAgICBAVmlld0NoaWxkKFwicmFkTGlzdDJcIikgcmFkbGlzdDI6IEVsZW1lbnRSZWY7XG4gICAgQFZpZXdDaGlsZChcInNjcm9sbFZpZXdcIikgc2Nyb2xsVmlldzogRWxlbWVudFJlZjtcbiAgICBAVmlld0NoaWxkKFwic3RhY2tMYXlvdXQxXCIpIHN0YWNrTGF5b3V0MTogRWxlbWVudFJlZjtcbiAgICBAVmlld0NoaWxkKFwic3RhY2tMYXlvdXQyXCIpIHN0YWNrTGF5b3V0MjogRWxlbWVudFJlZjtcblxuICAgIGNvbnN0cnVjdG9yKFxuICAgICAgICBwcml2YXRlIGl0ZW1TZXJ2aWNlOiBJdGVtU2VydmljZSxcbiAgICAgICAgcHJpdmF0ZSBwYWdlOiBQYWdlLFxuICAgICAgICBwcml2YXRlIHJvdXRlcjogUm91dGVyXG4gICAgICAgICkgeyB9XG5cbiAgICBuZ09uSW5pdCgpOiB2b2lkIHtcbiAgICAgICAgLy9Jbml0aWFsaXNpbmcgcHJvcGVydGllc1xuICAgICAgICB0aGlzLnNob3dGb290ZXIgPSBmYWxzZTtcbiAgICAgICAgdGhpcy5zaG93SGVhZGVyID0gZmFsc2U7XG4gICAgICAgIHRoaXMuZm9vdGVyTG9jYXRpb24gPSAwO1xuICAgICAgICB0aGlzLmhlYWRlckxvY2F0aW9uID0gMDtcbiAgICAgICAgdGhpcy50YXBwZWRJdGVtID0gLTE7XG4gICAgICAgIHRoaXMucGFnZS5hY3Rpb25CYXJIaWRkZW4gPSB0cnVlO1xuICAgICAgICAvL0ZldGNoaW5nIHRoZSBkYXRhIGl0ZW1zXG4gICAgICAgIHRoaXMuaXRlbVNlcnZpY2UuZ2V0SXRlbXMoKVxuICAgICAgICAudGhlbigoaXRlbXMpID0+IHtcbiAgICAgICAgICAgIHRoaXMuZnV0dXJlSXRlbXMgPSBuZXcgQmVoYXZpb3JTdWJqZWN0KGl0ZW1zLmZpbHRlcihpdGVtID0+IGl0ZW0udHlwZSA9PT0gXCJmdXR1cmVcIikpO1xuICAgICAgICB9KTtcbiAgICAgICAgdGhpcy5pdGVtU2VydmljZS5nZXRJdGVtcygpXG4gICAgICAgIC50aGVuKChpdGVtcykgPT4ge1xuICAgICAgICAgICAgdGhpcy5wYXN0SXRlbXMgPSBuZXcgQmVoYXZpb3JTdWJqZWN0KGl0ZW1zLmZpbHRlcihpdGVtID0+IGl0ZW0udHlwZSA9PT0gXCJwYXN0XCIpKTtcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgbmdBZnRlclZpZXdJbml0KCkge1xuICAgICAgICB0aGlzLmFic29sdXRlTGF5b3V0T2JqID0gPEFic29sdXRlTGF5b3V0PiB0aGlzLmFic29sdXRlTGF5b3V0Lm5hdGl2ZUVsZW1lbnQ7XG4gICAgICAgIHRoaXMucmFkTGlzdDFPYmogPSA8UmFkTGlzdFZpZXc+IHRoaXMucmFkbGlzdDEubmF0aXZlRWxlbWVudDtcbiAgICAgICAgdGhpcy5yYWRMaXN0Mk9iaiA9IDxSYWRMaXN0Vmlldz4gdGhpcy5yYWRsaXN0Mi5uYXRpdmVFbGVtZW50O1xuICAgICAgICB0aGlzLnNjcm9sbFZpZXdPYmogPSA8U2Nyb2xsVmlldz4gdGhpcy5zY3JvbGxWaWV3Lm5hdGl2ZUVsZW1lbnQ7XG4gICAgICAgIHRoaXMuc3RhY2tMYXlvdXQxT2JqID0gPFN0YWNrTGF5b3V0PiB0aGlzLnN0YWNrTGF5b3V0MS5uYXRpdmVFbGVtZW50O1xuICAgICAgICB0aGlzLnN0YWNrTGF5b3V0Mk9iaiA9IDxTdGFja0xheW91dD4gdGhpcy5zdGFja0xheW91dDIubmF0aXZlRWxlbWVudDtcblxuICAgICAgICB0aGlzLnBhZ2Uub24oUGFnZS5uYXZpZ2F0ZWRGcm9tRXZlbnQsIGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgdGhpcy5yZW1vdmVFdmVudExpc3RlbmVycygpO1xuICAgICAgICAgICAgY29uc29sZS5sb2coXCJOYXZpZ2F0ZWQgZnJvbSBpdGVtcy1jb21wb25lbnQuLi5cIik7XG4gICAgICAgIH0sIHRoaXMpO1xuXG4gICAgICAgIHRoaXMucGFnZS5vbihQYWdlLm5hdmlnYXRlZFRvRXZlbnQsIGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgdGhpcy5zZXRFdmVudExpc3RlbmVycygpO1xuICAgICAgICAgICAgY29uc29sZS5sb2coXCJOYXZpZ2F0ZWQgdG8gaXRlbXMtY29tcG9uZW50Li4uXCIpO1xuICAgICAgICB9LCB0aGlzKTtcblxuICAgICAgICBsZXQgd2VidmlldzogV2ViVmlldyA9IHRoaXMud2ViVmlld1JlZi5uYXRpdmVFbGVtZW50O1xuICAgICAgICBsZXQgbGFiZWw6IExhYmVsID0gdGhpcy5sYWJlbFJlc3VsdFJlZi5uYXRpdmVFbGVtZW50O1xuICAgICAgICBsYWJlbC50ZXh0ID0gXCJXZWJWaWV3IGlzIHN0aWxsIGxvYWRpbmcuLi5cIjtcblxuICAgICAgICB3ZWJ2aWV3Lm9uKFdlYlZpZXcubG9hZEZpbmlzaGVkRXZlbnQsIGZ1bmN0aW9uIChhcmdzOiBMb2FkRXZlbnREYXRhKSB7XG4gICAgICAgICAgICBsZXQgbWVzc2FnZTtcbiAgICAgICAgICAgIGlmICghYXJncy5lcnJvcikge1xuICAgICAgICAgICAgICAgIG1lc3NhZ2UgPSBcIldlYlZpZXcgZmluaXNoZWQgbG9hZGluZyBvZiBcIiArIGFyZ3MudXJsO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICBtZXNzYWdlID0gXCJFcnJvciBsb2FkaW5nIFwiICsgYXJncy51cmwgKyBcIjogXCIgKyBhcmdzLmVycm9yO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBsYWJlbC50ZXh0ID0gbWVzc2FnZTtcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiV2ViVmlldyBtZXNzYWdlIC0gXCIgKyBtZXNzYWdlKTtcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgcHVibGljIG9uU2Nyb2xsKGFyZ3M6IFNjcm9sbEV2ZW50RGF0YSkge1xuICAgICAgICAvL0xvZ2ljIGZvciBkaXNwbGF5aW5nIGhlYWRlciBhbmQgZm9vdGVyXG4gICAgICAgIHRoaXMuc2V0SGVhZGVyQW5kRm9vdGVyTG9jYXRpb24oKTtcblxuICAgICAgICAvL1RvIGhhbmRsZSBvcmllbnRhdGlvbiBjaGFuZ2VzXG4gICAgICAgIGlmICh0aGlzLm9yaWVudGF0aW9uQ2hhbmdlZCkge1xuICAgICAgICAgICAgLy9Mb2NhdGlvbiBjaGFuZ2VzIGluIG9yZGVyIHRvIGhpZGUgaGVhZGVyIGFuZCBmb290ZXJcbiAgICAgICAgICAgIGxldCB0aGF0ID0gdGhpcztcbiAgICAgICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24oKSB7XG4gICAgICAgICAgICAgICAgdGhhdC5oZWFkZXJMb2NhdGlvbiA9IC0xMDA7XG4gICAgICAgICAgICAgICAgdGhhdC5mb290ZXJMb2NhdGlvbiA9IC0xMDA7XG4gICAgICAgICAgICAgICAgdGhhdC5zZXRIZWFkZXJBbmRGb290ZXJMb2NhdGlvbigpO1xuICAgICAgICAgICAgfSwgMTAwKTsgLy9DYWxsZWQgd2l0aCBhIGRlbGF5IHRvIHByZXZlbnQgcmUtbG9jYXRpb24gcHJvYmxlbXNcbiAgICAgICAgICAgIHRoaXMub3JpZW50YXRpb25DaGFuZ2VkID0gZmFsc2U7XG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcIk9yaWVudGF0aW9uIGNoYW5nZXMgc2V0Li4uXCIpO1xuICAgICAgICB9XG4gICAgICAgIGNvbnNvbGUubG9nKFwic2Nyb2xsWTogXCIgKyBhcmdzLnNjcm9sbFkpO1xuICAgIH1cblxuICAgIHNldEhlYWRlckFuZEZvb3RlckxvY2F0aW9uKCkge1xuICAgICAgICAvL0hlYWRlciBsb2NhdGlvbiBsb2dpY1xuICAgICAgICBpZiAodGhpcy5zdGFja0xheW91dDJPYmouZ2V0TG9jYXRpb25SZWxhdGl2ZVRvKHRoaXMuc2Nyb2xsVmlld09iaikueSA8IHRoaXMuc2Nyb2xsVmlld09iai5nZXRMb2NhdGlvblJlbGF0aXZlVG8odGhpcy5hYnNvbHV0ZUxheW91dE9iaikueSkge1xuICAgICAgICAgICAgdGhpcy5zaG93SGVhZGVyID0gdHJ1ZTtcbiAgICAgICAgICAgIHRoaXMuaGVhZGVyTG9jYXRpb24gPSB0aGlzLnNjcm9sbFZpZXdPYmouZ2V0TG9jYXRpb25SZWxhdGl2ZVRvKHRoaXMuYWJzb2x1dGVMYXlvdXRPYmopLnk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICB0aGlzLnNob3dIZWFkZXIgPSBmYWxzZTtcbiAgICAgICAgfVxuXG4gICAgICAgIC8vRm9vdGVyIGxvY2F0aW9uIExvZ2ljXG4gICAgICAgIGlmICh0aGlzLnN0YWNrTGF5b3V0MU9iai5nZXRMb2NhdGlvblJlbGF0aXZlVG8odGhpcy5zY3JvbGxWaWV3T2JqKS55ID4gKHRoaXMuc2Nyb2xsVmlld09iai5nZXRBY3R1YWxTaXplKCkuaGVpZ2h0IC0gdGhpcy5zdGFja0xheW91dDFPYmouZ2V0QWN0dWFsU2l6ZSgpLmhlaWdodCkpIHtcbiAgICAgICAgICAgIHRoaXMuc2hvd0Zvb3RlciA9IHRydWU7XG4gICAgICAgICAgICB0aGlzLmZvb3RlckxvY2F0aW9uID0gdGhpcy5zY3JvbGxWaWV3T2JqLmdldEFjdHVhbFNpemUoKS5oZWlnaHQgLSB0aGlzLnN0YWNrTGF5b3V0MU9iai5nZXRBY3R1YWxTaXplKCkuaGVpZ2h0O1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgdGhpcy5zaG93Rm9vdGVyID0gZmFsc2U7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBvbkl0ZW1UYXBGdXR1cmUoYXJnczogTGlzdFZpZXdFdmVudERhdGEpIHtcbiAgICAgICAgLy9UbyBoYW5kbGUgZnV0dXJlSXRlbXMgdGFwIGV2ZW50c1xuICAgICAgICBsZXQgaXRlbXMgPSB0aGlzLmZ1dHVyZUl0ZW1zLmdldFZhbHVlKCk7XG4gICAgICAgIHRoaXMudGFwcGVkSXRlbSA9IGl0ZW1zW2FyZ3MuaXRlbUluZGV4XS5pZDtcbiAgICAgICAgbGV0IHRoYXQgPSB0aGlzO1xuICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgdGhhdC50YXBwZWRJdGVtID0gLTE7XG4gICAgICAgICAgICB0aGF0LnJvdXRlci5uYXZpZ2F0ZShbXCIvaXRlbVwiLCBpdGVtc1thcmdzLml0ZW1JbmRleF0uaWRdKTtcbiAgICAgICAgfSwgMTApO1xuICAgICAgICBjb25zb2xlLmxvZyhcIkZ1dHVyZSBpdGVtIHRhcHBlZFwiKTtcbiAgICB9XG5cbiAgICBvbkl0ZW1UYXBQYXN0KGFyZ3M6IExpc3RWaWV3RXZlbnREYXRhKSB7XG4gICAgICAgIC8vVG8gaGFuZGxlIHBhc3RJdGVtcyB0YXAgZXZlbnRzXG4gICAgICAgIGxldCBpdGVtcyA9IHRoaXMucGFzdEl0ZW1zLmdldFZhbHVlKCk7XG4gICAgICAgIHRoaXMudGFwcGVkSXRlbSA9IGl0ZW1zW2FyZ3MuaXRlbUluZGV4XS5pZDtcbiAgICAgICAgbGV0IHRoYXQgPSB0aGlzO1xuICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgdGhhdC50YXBwZWRJdGVtID0gLTE7XG4gICAgICAgICAgICB0aGF0LnJvdXRlci5uYXZpZ2F0ZShbXCIvaXRlbVwiLCBpdGVtc1thcmdzLml0ZW1JbmRleF0uaWRdKTtcbiAgICAgICAgfSwgMTApO1xuICAgICAgICBjb25zb2xlLmxvZyhcIlBhc3QgaXRlbSB0YXBwZWRcIik7XG4gICAgfVxuXG4gICAgc2hvd0xvZyhtZXNzYWdlOiBzdHJpbmcpIHtcbiAgICAgICAgLy9UbyBoYW5kbGUgaGVhZGVyIGFuZCBmb290ZXIgdGFwIGV2ZW50c1xuICAgICAgICBjb25zb2xlLmxvZyhtZXNzYWdlKTtcbiAgICB9XG5cbiAgICBzZXRFdmVudExpc3RlbmVycygpIHtcbiAgICAgICAgLy9SZWJpbmRpbmcgdGhlIGxpc3QgaXRlbXNcbiAgICAgICAgYXBwbGljYXRpb24ub24oYXBwbGljYXRpb24ucmVzdW1lRXZlbnQsIGZ1bmN0aW9uKCkge1xuICAgICAgICAgICAgLy9UaGlzIGlzIG5lY2Vzc2FyeSBzbyBhcyB0byByZXN0b3JlIHRoZSB0YXAgZXZlbnQgYmluZGluZ3NcbiAgICAgICAgICAgIGFwcGxpY2F0aW9uLm9uKGFwcGxpY2F0aW9uLm9yaWVudGF0aW9uQ2hhbmdlZEV2ZW50LCBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgICAgICAvKlxuICAgICAgICAgICAgICAgICAgICBUaGlzIGlzIG5lY2Vzc2FyeSBhcyB3aGVuIHRoZSBvcmllbnRhdGlvbiBjaGFuZ2VzLCBcbiAgICAgICAgICAgICAgICAgICAgdGhlIGZvb3Rlci9oZWFkZXIgbWlnaHQgbm90IGdldCByZS1sb2NhdGVkIHByb3Blcmx5LlxuICAgICAgICAgICAgICAgICovXG4gICAgICAgICAgICAgICAgbGV0IHNjcm9sbDEgPSB0aGlzLnJhZExpc3QxT2JqLmdldExvY2F0aW9uUmVsYXRpdmVUbyh0aGlzLnNjcm9sbFZpZXdPYmopLnk7XG4gICAgICAgICAgICAgICAgaWYgKHNjcm9sbDEgPCAwKSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc2Nyb2xsVmlld09iai5zY3JvbGxUb1ZlcnRpY2FsT2Zmc2V0KE1hdGguYWJzKHNjcm9sbDEgKyAxKSk7XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zY3JvbGxWaWV3T2JqLnNjcm9sbFRvVmVydGljYWxPZmZzZXQoc2Nyb2xsMSsxKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgdGhpcy5vcmllbnRhdGlvbkNoYW5nZWQgPSB0cnVlO1xuICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiT3JpZW50YXRpb24gY2hhbmdlZC4uLlwiKTtcbiAgICAgICAgICAgIH0sIHRoaXMpO1xuICAgICAgICAgICAgdGhpcy5yYWRMaXN0MU9iai5yZWZyZXNoKCk7XG4gICAgICAgICAgICB0aGlzLnJhZExpc3QyT2JqLnJlZnJlc2goKTtcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiQXBwbGljYXRpb24gcmVzdW1lZC4uLlwiKTtcbiAgICAgICAgfSwgdGhpcyk7XG5cbiAgICAgICAgYXBwbGljYXRpb24ub24oYXBwbGljYXRpb24uc3VzcGVuZEV2ZW50LCBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIGFwcGxpY2F0aW9uLm9mZihhcHBsaWNhdGlvbi5vcmllbnRhdGlvbkNoYW5nZWRFdmVudCk7XG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcIkFwcGxpY2F0aW9uIHN1c3BlbmRlZC4uLlwiKTtcbiAgICAgICAgfSwgdGhpcyk7XG5cbiAgICAgICAgLy9IYW5kbGluZyBvcmllbnRhdGlvbiBjaGFuZ2VzXG4gICAgICAgIGFwcGxpY2F0aW9uLm9uKGFwcGxpY2F0aW9uLm9yaWVudGF0aW9uQ2hhbmdlZEV2ZW50LCBmdW5jdGlvbigpIHtcbiAgICAgICAgICAgIC8qXG4gICAgICAgICAgICAgICAgVGhpcyBpcyBuZWNlc3NhcnkgYXMgd2hlbiB0aGUgb3JpZW50YXRpb24gY2hhbmdlcywgXG4gICAgICAgICAgICAgICAgdGhlIGZvb3Rlci9oZWFkZXIgbWlnaHQgbm90IGdldCByZS1sb2NhdGVkIHByb3Blcmx5LlxuICAgICAgICAgICAgKi9cbiAgICAgICAgICAgIGxldCBzY3JvbGwxID0gdGhpcy5yYWRMaXN0MU9iai5nZXRMb2NhdGlvblJlbGF0aXZlVG8odGhpcy5zY3JvbGxWaWV3T2JqKS55O1xuICAgICAgICAgICAgaWYgKHNjcm9sbDEgPCAwKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5zY3JvbGxWaWV3T2JqLnNjcm9sbFRvVmVydGljYWxPZmZzZXQoTWF0aC5hYnMoc2Nyb2xsMSArIDEpKTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgdGhpcy5zY3JvbGxWaWV3T2JqLnNjcm9sbFRvVmVydGljYWxPZmZzZXQoc2Nyb2xsMSsxKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHRoaXMub3JpZW50YXRpb25DaGFuZ2VkID0gdHJ1ZTtcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiT3JpZW50YXRpb24gY2hhbmdlZC4uLlwiKTtcbiAgICAgICAgfSwgdGhpcyk7XG4gICAgfVxuXG4gICAgcmVtb3ZlRXZlbnRMaXN0ZW5lcnMoKSB7XG4gICAgICAgIGFwcGxpY2F0aW9uLm9mZihhcHBsaWNhdGlvbi5vcmllbnRhdGlvbkNoYW5nZWRFdmVudCk7XG4gICAgICAgIGFwcGxpY2F0aW9uLm9mZihhcHBsaWNhdGlvbi5yZXN1bWVFdmVudCk7XG4gICAgICAgIGFwcGxpY2F0aW9uLm9mZihhcHBsaWNhdGlvbi5zdXNwZW5kRXZlbnQpO1xuICAgIH1cblxuXG4gICAgZ29CYWNrKCkge1xuICAgICAgICBsZXQgd2VidmlldzogV2ViVmlldyA9IHRoaXMud2ViVmlld1JlZi5uYXRpdmVFbGVtZW50O1xuICAgICAgICBpZiAod2Vidmlldy5jYW5Hb0JhY2spIHtcbiAgICAgICAgICAgIHdlYnZpZXcuZ29CYWNrKCk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBzdWJtaXQoYXJnczogc3RyaW5nKSB7XG4gICAgICAgIGxldCB0ZXh0RmllbGQ6IFRleHRGaWVsZCA9IHRoaXMudXJsRmllbGRSZWYubmF0aXZlRWxlbWVudDtcblxuICAgICAgICBpZiAoYXJncy5zdWJzdHJpbmcoMCwgNCkgPT09IFwiaHR0cFwiKSB7XG4gICAgICAgICAgICB0aGlzLndlYlZpZXdTcmMgPSBhcmdzO1xuICAgICAgICAgICAgdGV4dEZpZWxkLmRpc21pc3NTb2Z0SW5wdXQoKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIGFsZXJ0KFwiUGxlYXNlLCBhZGQgYGh0dHA6Ly9gIG9yIGBodHRwczovL2AgaW4gZnJvbnQgb2YgdGhlIFVSTCBzdHJpbmdcIik7XG4gICAgICAgIH1cbiAgICB9XG59XG4iXX0=