import { Injectable } from "@angular/core";

import { Item } from "./item";

@Injectable()
export class ItemService {
    private items = new Array<Item>(
        { id: 1, name: "Ter Stegen", role: "Goalkeeper", type: "future" },
        { id: 2, name: "Hawkridge", role: "Goalkeeper", type: "future" },
        { id: 3, name: "Piqué", role: "Defender", type: "future" },
        { id: 4, name: "I. Rakitic", role: "Midfielder", type: "future" },
        { id: 5, name: "Sergio", role: "Midfielder", type: "future" },
        { id: 6, name: "Denis Suárez", role: "Midfielder", type: "future" },
        { id: 7, name: "Arda", role: "Midfielder", type: "future" },
        { id: 8, name: "A. Iniesta", role: "Midfielder", type: "future" },
        { id: 9, name: "Suárez", role: "Forward", type: "future" },
        { id: 10, name: "Messi", role: "Forward", type: "future" },
        { id: 11, name: "Neymar", role: "Forward", type: "future" },
        { id: 12, name: "Rafinha", role: "Midfielder", type: "future" },
        { id: 13, name: "Cillessen", role: "Goalkeeper", type: "future" },
        { id: 14, name: "Mascherano", role: "Defender", type: "future" },
        { id: 15, name: "Dechelle", role: "Forward", type: "future" }, 
        { id: 16, name: "Rowlett", role: "Defender", type: "future" },
        { id: 17, name: "Paco Alcácer", role: "Forward", type: "future" },
        { id: 18, name: "Jordi Alba", role: "Defender", type: "future" },
        { id: 19, name: "Digne", role: "Defender", type: "future" },
        { id: 20, name: "Sergi Roberto", role: "Midfielder", type: "future" },
        { id: 21, name: "André Gomes", role: "Midfielder", type: "past" },
        { id: 22, name: "Aleix Vidal", role: "Midfielder", type: "past" },
        { id: 23, name: "Umtiti", role: "Defender", type: "past" },
        { id: 24, name: "Mathieu", role: "Defender", type: "past" },
        { id: 25, name: "Masip", role: "Goalkeeper", type: "past" },
        { id: 26, name: "Borgesio", role: "Defender", type: "past" },
        {
            id: 27,
            name: "Walesa",
            role: "Midfielder",
            type: "past"
        }, 
        {
            id: 28,
            name: "Adshead",
            role: "Defender",
            type: "past"
            }, 
        {
            id: 29,
            name: "Cahan",
            role: "Forward",
            type: "past"
        }, 
        {
            id: 30,
            name: "Saunier",
            role: "Forward",
            type: "past"
        }, 
        {
            id: 31,
            name: "Tomasz",
            role: "Forward",
            type: "past"
        }, 
        {
            id: 32,
            name: "Gellert",
            role: "Forward",
            type: "past"
        }, 
        {
            id: 33,
            name: "Bellard",
            role: "Goalkeeper",
            type: "past"
        }, 
        {
            id: 34,
            name: "Farnworth",
            role: "Midfielder",
            type: "past"
        }, 
        {
            id: 35,
            name: "Livesley",
            role: "Forward",
            type: "past"
        }, 
        {
            id: 36,
            name: "Lyne",
            role: "Forward",
            type: "past"
        }, 
        {
            id: 37,
            name: "Prowting",
            role: "Forward",
            type: "past"
        }, 
        {
            id: 38,
            name: "Dowse",
            role: "Forward",
            type: "past"
        }, 
        {
            id: 39,
            name: "Weond",
            role: "Goalkeeper",
            type: "past"
        }, 
        {
            id: 40,
            name: "Kernley",
            role: "Defender",
            type: "past"
        }   
    );

    getItems(): Promise<Item[]> {
        return new Promise((resolve) => resolve(this.items));
    }

    getItem(id: number): Item {
        return this.items.filter(item => item.id === id)[0];
    }
}
